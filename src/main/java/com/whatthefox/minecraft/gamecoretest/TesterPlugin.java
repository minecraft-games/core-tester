package com.whatthefox.minecraft.gamecoretest;

import com.whatthefox.minecraft.gamecore.entity.GameManagerInfo;
import com.whatthefox.minecraft.gamecore.entity.GameState;
import com.whatthefox.minecraft.gamecore.manager.GameCoreManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

public class TesterPlugin implements CommandExecutor {

  @Override
  public boolean onCommand(@NotNull final CommandSender commandSender, @NotNull final Command cmd, @NotNull final String details,
      final @NotNull String[] args) {
    // Example code
    // Do not use this in command context
    if (GameCoreManager.getState() == GameState.INIT) {
      commandSender.sendMessage("Calling GameCoreManager init() and subscribe()");
      GameCoreManager.init(new GameManagerInfo(1, 5, Bukkit.getWorlds().get(0).getSpawnLocation(), "CoreTest"));
      GameCoreManager.subscribe(GameCoreTestPlugin.getInstance());
    } else if (GameCoreManager.getState() == GameState.IN_PROGRESS) {
      commandSender.sendMessage("Calling GameCoreManager endGame()");
      GameCoreManager.endGame();
    } else {
      commandSender.sendMessage("There is nothing to do");
    }
    return false;
  }
}
