package com.whatthefox.minecraft.gamecoretest;

import com.whatthefox.minecraft.gamecore.entity.Observer;
import com.whatthefox.minecraft.gamecore.entity.RepeatingTask;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class GameCoreTestPlugin extends JavaPlugin implements Observer {

  public static GameCoreTestPlugin getInstance() {
    return getPlugin(GameCoreTestPlugin.class);
  }

  @Override
  public void onEnable() {
    this.getCommand("tester").setExecutor(new TesterPlugin());
  }

  @Override
  public void startGame(final List<Player> players, final List<Player> spectators) {
    Bukkit.broadcastMessage("Début de la partie !");
    Bukkit.broadcastMessage("");
    getLogger().info("Receiving event startGame()");
    getLogger().info("Players: " + players.stream().map(Player::getName).toList());
    getLogger().info("Spectators: " + players.stream().map(Player::getName).toList());
    startCountdown();
  }

  private void startCountdown() {
    Bukkit.broadcastMessage("Voici un exemple de timer");
    getLogger().info("Starting countdown");
    new RepeatingTask(this, 0, 20) {
      private final int max = 3;
      private int count = max;

      @Override
      public void run() {
        if (count == 0) {
          Bukkit.broadcastMessage("Fin de l'exemple !");
          Bukkit.broadcastMessage("");
          Bukkit.broadcastMessage("Pour terminer la partie, utilisez la commande /tester");
          cancelLoop();
        } else {
          Bukkit.broadcastMessage("Fin de l'exemple dans " + count + " secondes");
          count--;
        }
      }
    };
  }
}
